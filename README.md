# Open-Anais

> Open-Anais is a Node library that allows to register visitors of french Assemblée nationale.

Internaly Open-Anais uses the Anais extranet web application and converts it to a library.

Open-Anais is a component of the french [Open Parliament Platform](https://forum.parlement-ouvert.fr).

Open-Anais is free and open source software, developped by the french Member of Parliament [Paula Forteza](https://forteza.fr) and her team.

## Usage

See [/src/examples](/src/examples).
