/**
 * Open-Anais -- Library to register visitors of french Assemblée nationale
 * By: Paula Forteza <paula@forteza.fr>
 *     Emmanuel Raviart <emmanuel@raviart.com>
 *
 * Copyright (C) 2017 Paula Forteza & Emmanuel Raviart
 * https://framagit.org/parlement-ouvert/open-anais
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { loginPage, puppeteer, registerVisitor, setVisit } from ".."

import config from "./config"

(async ({ username, password, ...puppeteerConfig }) => {
  const browser = await puppeteer.launch(puppeteerConfig)
  const page = await loginPage(browser, username, password)
  await setVisit(page, "30/11/2017", "09", "30", "7426", "Jean Dupont 06 54 32 10 98", "bureau ouvert")
  await registerVisitor(page, "Mr", "Durand", "Louise", "31", "12", "2000", "Paris", "France")
  await browser.close()
})(config).catch(error => {
  console.log(error.stack || error)
  process.exit(1)
})
