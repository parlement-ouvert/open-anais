/**
 * Open-Anais -- Library to register visitors of french Assemblée nationale
 * By: Paula Forteza <paula@forteza.fr>
 *     Emmanuel Raviart <emmanuel@raviart.com>
 *
 * Copyright (C) 2017 Paula Forteza & Emmanuel Raviart
 * https://framagit.org/parlement-ouvert/open-anais
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { loginPortalFrame } from "authan"
import puppeteerImport from "puppeteer"
export const puppeteer = puppeteerImport

async function focusAndType(page, selector, text) {
  while (!await page.$(selector)) {
    await page.waitFor(50)
  }
  await page.click(selector)
  while (!await page.$(`${selector}.edited`)) {
    await page.waitFor(50)
  }
  await page.keyboard.down("ControlLeft")
  await page.keyboard.press("KeyA")
  await page.keyboard.up("ControlLeft")
  await page.type(selector, text)
  await page.waitFor(100)
}

export async function loginPage(browser, username, password) {
  const frame = await loginPortalFrame(browser, username, password)

  await frame.waitFor('a[alt="Declaration prealable a l’accueil des invites dans les locaux de l’Assemblee nationale"]')
  let elementHandle = await frame.$(
    'a[alt="Declaration prealable a l’accueil des invites dans les locaux de l’Assemblee nationale"]',
  )
  await elementHandle.click()

  let page
  for (let retries = 0; retries < 60; retries++) {
    await frame.waitFor(500)
    page = (await browser.pages()).filter(page => page.url() === "https://fm15-ext.assemblee-nationale.fr/fmi/webd")[0]
    if (page) {
      break
    }
  }
  if (!page) {
    return null
  }

  await page.waitFor("input[type=text]")
  await page.waitFor(500)
  await page.type("input[type=text]", username)
  await page.type("input[type=password]", password)
  await page.click("div.primary[role=button]")
  await page.waitFor("div#b0p1o147i0i0r1") // Date de la venue
  return page
}

export async function registerVisitor(
  page,
  civility,
  lastName,
  firstName,
  birthDay,
  birthMonth,
  birthYear,
  birthCity,
  birthCountry,
) {
  // Civilité
  console.log("Civilité :", civility)
  await focusAndType(page, "div#b0p1o164i0i0r1", civility)

  // Nom
  console.log("Nom :", lastName)
  await focusAndType(page, "div#b0p1o166i0i0r1", lastName)

  // Prénom
  console.log("Prénom :", firstName)
  await focusAndType(page, "div#b0p1o168i0i0r1", firstName)

  // Date de naissance
  console.log(`Date de naissance : ${birthDay}/${birthMonth}/${birthYear}`)
  await focusAndType(page, "div#b0p1o208i0i0r1", birthDay)
  await focusAndType(page, "div#b0p1o209i0i0r1", birthMonth)
  // The Filemaker server needs some additional delay here!
  await page.waitFor(1000)
  await focusAndType(page, "div#b0p1o210i0i0r1", birthYear)

  // Ville de naissance
  console.log("Ville de naissance :", birthCity)
  await focusAndType(page, "div#b0p1o172i0i0r1", birthCity)

  // Pays de naissance
  console.log("Pays de naissance :", birthCountry)
  await focusAndType(page, "div#b0p1o174i0i0r1", birthCountry)

  // Valider
  console.log("Valider")
  await page.click("button#b0p1o176i0i0r1")
  await page.waitFor(500)
}

export async function setVisit(page, date, hours, minutes, room, contact, object) {
  page.setViewport({
    height: 1280,
    width: 1900,
  })

  // Date de la venue
  console.log("Date de la venue :", date)
  await focusAndType(page, "div#b0p1o147i0i0r1", date)

  // Motif de la venue
  console.log("Motif de la venue")
  await page.click("div#b0p1o149i0i0r1")
  await page.waitFor("div#b0p1o145i0i0r1>div[role=group]>span:nth-child(3)>input[type=checkbox]")
  await page.click("div#b0p1o145i0i0r1>div[role=group]>span:nth-child(3)>input[type=checkbox]")

  // Heure
  console.log(`Heure : ${hours}h${minutes}`)
  await focusAndType(page, "div#b0p1o152i0i0r1", hours)
  // The Filemaker server needs some additional delay here!
  await focusAndType(page, "div#b0p1o152i0i0r1", hours)
  await focusAndType(page, "div#b0p1o217i0i0r1", minutes)

  // Lieu d'arrivée
  console.log("Lieu d'arrivée")
  await page.click("div#b0p1o155i0i0r1")
  await page.waitFor("div#b0p1o142i0i0r1>div[role=group]>span:nth-child(3)>input[type=checkbox]")
  await page.click("div#b0p1o142i0i0r1>div[role=group]>span:nth-child(3)>input[type=checkbox]")

  // Salle
  console.log("Salle :", room)
  await focusAndType(page, "div#b0p1o156i0i0r1", room)

  // Autre contact à prévenir (nom prénom téléphone)
  console.log("Autre contact à prévenir (nom prénom téléphone)")
  await focusAndType(page, "div#b0p1o265i0i0r1", contact)

  // Objet
  console.log("Objet :", object)
  await focusAndType(page, "div#b0p1o160i0i0r1", object)
}
